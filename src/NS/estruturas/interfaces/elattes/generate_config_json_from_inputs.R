#***
#testes iniciais para funcao de saida para json

library(lubridate)


generate_config_json_from_inputs <- function(globalInputs) {
  
  #Transformar lista de opcoes em sim e nao
  
  #solucao provisoria
  options <- globalInputs["publicationsInput"][[1]]
  jsonOptions <- c(
                    "incluir_artigo_em_periodico" ,
                    "incluir_livro_publicado" ,
                    "incluir_capitulo_de_livro_publicado" ,
                    "incluir_texto_em_jornal_de_noticia",
                    "incluir_trabalho_em_congresso" ,
                    "incluir_artigo_aceito_para_publicacao",
                    "incluir_outro_tipo_de_producao_bibliografica" 
                    )
  
  resultOptions <- rep("nao", 7)
  
  for(option in options) {
    
      resultOptions[agrep(option, jsonOptions, max.distance = 8)] <- "sim"
    
  }
  
  
  result <- list(
    
    global=
    list(
      arquivo_de_entrada = "aggeu.list.json",
      diretorio_de_saida = getwd(),
      intes_desde_o_ano = as.character(year(globalInputs["custom_period_filter"][[1]][1])),
      itens_ate_o_ano = as.character(year(globalInputs["custom_period_filter"][[1]][2])),
      identificador_publicacoes_com_qualis = "nao",
      arquivo_qualis_de_periodicos = "",
      diretorio_de_armazenamento_de_cvs = getwd()
    ),
    
    informacoes_complementares=
    list(
      incluir_projeto = "nao",
      incluir_premio = "nao",
      incluir_participacao_em_evento = "nao",
      incluir_organizacao_de_evento = "nao"
    ),
    
    grafo_de_coautoria=
      list(
        incluir_artigo_em_periodico = resultOptions[1],
        incluir_livro_publicado = resultOptions[2],
        incluir_capitulo_de_livro_publicado = resultOptions[3],
        incluir_texto_em_jornal_de_noticia = resultOptions[4],
        incluir_trabalho_em_congresso = resultOptions[5],
        incluir_artigo_aceito_para_publicacao = resultOptions[6],
        incluir_outro_tipo_de_producao_bibliografica = resultOptions[7]
      )
    
  )
  
  result<- toJSON(result,  pretty = TRUE, auto_unbox = TRUE)
  
  return(result)
}

