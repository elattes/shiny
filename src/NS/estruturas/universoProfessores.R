#***
#Este deverah ser um script de estrutura para preparar a tela inicial para receber as opcoes disponiveis de professores

teachers_to_choices <- c("Francisco José da Silva e Silva",   "Carlos de Salles Soares Neto",      "Samyr Beliche Vale"           ,     "Tiago Bonini Borchartt"           
 , "Aristófanes Corrêa Silva"   ,       "Marcos Antonio dos Santos"   ,      "Ricardo Barros Sampaio" ,           "Jesús Pascual Mena Chalco",        
  "Alexandre César Muniz de Oliveira", "Luciano Reis Coutinho"      ,       "João Dallyson Sousa de Almeida" ,   "Anselmo Cardoso de Paiva"   ,     
  "Areolino de Almeida Neto",          "Geraldo Braz Júnior",               "Antonio de Abreu Batista Junior",   "Mário Antonio Meireles Teixeira")

#***
#Este vetor abaixo deverah ser linkado aos ids de cada professor.
# 
# teachers_to_choices <- c(NULL) 
# for(i in 1:length(teachers)) {
# 
#   teachers_to_choices <- c(teachers_to_choices, teachers[[i]]$nome)
#   
# }


#Este seria para o dado geral, no entanto quando o usuario escolhe um escopo, deve haver um filtro por id. 