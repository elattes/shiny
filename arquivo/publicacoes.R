library(tidyverse)
library(jsonlite) 
library(listviewer)
library(igraph)
library(rlist)
library(dplyr)
library(ggplot2)
library(ggvis)    # Added


source("elattes.ls2df.R")  #Usamos funcoes dadas pelo professor Ricardo. Elas estao nesse "elattes.ls2df.R"

public <- fromJSON("json/publication.json")

#####################################################################################################

## Funcao que retorna os anos em analise
anos_em_analise <- function(){
  anos_em_analise <- vector()
    for(i in 1 : length(public$PERIODICO)){
      anos_em_analise <- c(anos_em_analise, names(public$PERIODICO)[i])
    }
  return(anos_em_analise)   #retorna anos em analise
}
anos_em_analise


######################################## PERIODICOS #######################################

public.periodico.df <- pub.ls2df(public, 1) # 1 = periodicos
table(public.periodico.df$ano)

# add_tooltip(function(df){
#   paste0("Ano: ",df$ano, "<br>", "Quantidade: ", df$stack_upr_ - df$stack_lwr_)
# }, "click") %>%

# Para controlar a opacidade pode-se usar opacity := input_slider(0, 1) dentro do ggvis

public.periodico.df %>%
  group_by(ano) %>%
  summarise(Quantidade = n()) %>%
  slice((n()-10):n()) %>% # Seleciona o range de anos (10 ultimos anos por padrao)
  ggvis(~ano, ~Quantidade, fill := "#006699", stroke := "#006699") %>% 
  layer_bars(width = .8) %>%
  layer_text(
    text:=~Quantidade, 
    prop("x", ~ano, scale = "xcenter"),
    y = ~Quantidade - Quantidade*0.02,
    fontSize := 20, 
    fill := "white", 
    stroke := "white", 
    baseline:="top", 
    align := "center") %>%
  scale_nominal("x", name = "xcenter", padding = .8, points = TRUE) %>%
  hide_axis("y") %>%
  add_axis("x", title = "", grid = TRUE, orient = "top", properties = axis_props(
    axis = list(stroke = "white"),
    ticks = list(stroke = "white"),
    labels = list(fill = "gray")
  ))

######################################## LIVROS #######################################

public.livros.df <- pub.ls2df(public, 2) # 2 = livros

public.livros.df %>%
  group_by(ano) %>%
  summarise(Quantidade = n()) %>%
  slice((n()-10):n()) %>% # Seleciona o range de anos (10 ultimos anos por padrao)
  ggvis(~ano, ~Quantidade, fill := "#006699", stroke := "#006699") %>% 
  layer_bars(width = .8) %>%
  layer_text(
    text:=~Quantidade, 
    prop("x", ~ano, scale = "xcenter"),
    y = ~Quantidade - Quantidade*0.02,
    fontSize := 20, 
    fill := "white", 
    stroke := "white", 
    baseline:="top", 
    align := "center") %>%
  scale_nominal("x", name = "xcenter", padding = .8, points = TRUE) %>%
  hide_axis("y") %>%
  add_axis("x", title = "", grid = TRUE, orient = "top", properties = axis_props(
    axis = list(stroke = "white"),
    ticks = list(stroke = "white"),
    labels = list(fill = "gray")
  ))

################################# CAPITULOS DE LIVROS ###############################

public.capitulo.livros.df <- pub.ls2df(public, 3) # 3 = capitulo de livro

public.capitulo.livros.df %>%
  group_by(ano) %>%
  summarise(Quantidade = n()) %>%
  slice((n()-10):n()) %>% # Seleciona o range de anos (10 ultimos anos por padrao)
  ggvis(~ano, ~Quantidade, fill := "#006699", stroke := "#006699") %>% 
  layer_bars(width = .8) %>%
  layer_text(
    text:=~Quantidade, 
    prop("x", ~ano, scale = "xcenter"),
    y = ~Quantidade - Quantidade*0.02,
    fontSize := 20, 
    fill := "white", 
    stroke := "white", 
    baseline:="top", 
    align := "center") %>%
  scale_nominal("x", name = "xcenter", padding = .8, points = TRUE) %>%
  hide_axis("y") %>%
  add_axis("x", title = "", grid = TRUE, orient = "top", properties = axis_props(
    axis = list(stroke = "white"),
    ticks = list(stroke = "white"),
    labels = list(fill = "gray")
  ))

############################## TEXTO EM JORNAIS ###################################

public.texto.jornais.df <- pub.ls2df(public, 4) # 4 = texto em jornais

public.texto.jornais.df %>%
  group_by(ano) %>%
  summarise(Quantidade = n()) %>%
  slice((n()-10):n()) %>% # Seleciona o range de anos (10 ultimos anos por padrao)
  ggvis(~ano, ~Quantidade, fill := "#006699", stroke := "#006699") %>% 
  layer_bars(width = .8) %>%
  layer_text(
    text:=~Quantidade, 
    prop("x", ~ano, scale = "xcenter"),
    y = ~Quantidade - Quantidade*0.02,
    fontSize := 20, 
    fill := "white", 
    stroke := "white", 
    baseline:="top", 
    align := "center") %>%
  scale_nominal("x", name = "xcenter", padding = .8, points = TRUE) %>%
  hide_axis("y") %>%
  add_axis("x", title = "", grid = TRUE, orient = "top", properties = axis_props(
    axis = list(stroke = "white"),
    ticks = list(stroke = "white"),
    labels = list(fill = "gray")
  ))

################################# EVENTOS ####################################

public.eventos.df <- pub.ls2df(public, 5) # 5 = eventos

public.eventos.df %>%
  group_by(ano_do_trabalho) %>%
  summarise(Quantidade = n()) %>%
  slice((n()-10):n()) %>% # Seleciona o range de anos (10 ultimos anos por padrao)
  ggvis(~ano_do_trabalho, ~Quantidade, fill := "#006699", stroke := "#006699") %>% 
  layer_bars(width = .8) %>%
  layer_text(
    text:=~Quantidade, 
    prop("x", ~ano_do_trabalho, scale = "xcenter"),
    y = ~Quantidade - Quantidade*0.02,
    fontSize := 20, 
    fill := "white", 
    stroke := "white", 
    baseline:="top", 
    align := "center") %>%
  scale_nominal("x", name = "xcenter", padding = .8, points = TRUE) %>%
  hide_axis("y") %>%
  add_axis("x", title = "", grid = TRUE, orient = "top", properties = axis_props(
    axis = list(stroke = "white"),
    ticks = list(stroke = "white"),
    labels = list(fill = "gray")
  ))

############################### ARTIGOS ACEITOS ###################################
public.artigos.aceitos.df <- pub.ls2df(public, 6) # 6 = artigos aceitos

public.artigos.aceitos.df %>%
  group_by(ano) %>%
  summarise(Quantidade = n()) %>%
  slice((n()-10):n()) %>% # Seleciona o range de anos (10 ultimos anos por padrao)
  ggvis(~ano, ~Quantidade, fill := "#006699", stroke := "#006699") %>% 
  layer_bars(width = .8) %>%
  layer_text(
    text:=~Quantidade, 
    prop("x", ~ano, scale = "xcenter"),
    y = ~Quantidade - Quantidade*0.02,
    fontSize := 20, 
    fill := "white", 
    stroke := "white", 
    baseline:="top", 
    align := "center") %>%
  scale_nominal("x", name = "xcenter", padding = .8, points = TRUE) %>%
  hide_axis("y") %>%
  add_axis("x", title = "", grid = TRUE, orient = "top", properties = axis_props(
    axis = list(stroke = "white"),
    ticks = list(stroke = "white"),
    labels = list(fill = "gray")
  ))

############################### DEMAIS TIPOS #################################

public.demais.tipos.df <- pub.ls2df(public, 7) # 7 = demais tipos

public.demais.tipos.df %>%
  group_by(ano) %>%
  summarise(Quantidade = n()) %>%
  slice((n()-10):n()) %>% # Seleciona o range de anos (10 ultimos anos por padrao)
  ggvis(~ano, ~Quantidade, fill := "#006699", stroke := "#006699") %>% 
  layer_bars(width = .8) %>%
  layer_text(
    text:=~Quantidade, 
    prop("x", ~ano, scale = "xcenter"),
    y = ~Quantidade - Quantidade*0.02,
    fontSize := 20, 
    fill := "white", 
    stroke := "white", 
    baseline:="top", 
    align := "center") %>%
  scale_nominal("x", name = "xcenter", padding = .8, points = TRUE) %>%
  hide_axis("y") %>%
  add_axis("x", title = "", grid = TRUE, orient = "top", properties = axis_props(
    axis = list(stroke = "white"),
    ticks = list(stroke = "white"),
    labels = list(fill = "gray")
  ))
  
