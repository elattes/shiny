##########################################################################################
# Este arquivo contem as visualizacoes referentes ao dataset perfis.json 
##########################################################################################

# Bibliotecas utilizadas
library(tidyverse)
library(jsonlite) 
library(listviewer)
library(igraph)
library(rlist)
library(dplyr)
library(tidyr)
library(ggplot2)
library(ggvis)   
library(wordcloud)
library(ggmap)


source("elattes.ls2df.R")  # Usamos funcoes dadas pelo professor Ricardo. Elas estão nesse "elattes.ls2df.R"

perfil <- fromJSON("json/profile.json")

perfil.df <- extrai.perfis(perfil)   #Extracao dos perfis do arquivo json

perfil.areas.df <- extrai.areas.atuacao(perfil) # Extracao das areas de atuacao do arquivo json



####################### MAPA ##########################################

constroi_mapa_tematico <- function(dataset){
  dataset %>% 
    inner_join(br_uf_map) %>% {
      ggplot(.) +
        geom_map(aes(x = long, y = lat,
                     map_id = id, fill = variavel),
                 color = 'gray30', map = ., data = .) + 
        theme_void() +
        coord_equal()
    }
}

media_ddd <- as.data.frame(table(perfil.df$DDD)) #transforma em DF



filtra_ddd <- function(media_ddd){
  media_ddd$Var1 <- plyr::revalue(media_ddd$Var1, c('011'=11, '012'=12, '013'=13, '014'=14, '015'=15, '016'=16, '017'=17, '018'=18, 
                                                    '019'=19, '021'=21, '022'=22, '024'=24, '027'=27, '028'=28, '031'=31, '032'=32, '033'=33,
                                                    '034'=34, '035'=35, '037'=37, '038'=38, '041'=41, '042'=42, '043'=43, '044'=44, '045'=45,
                                                    '046'=46, '047'=47, '048'=48, '049'=49, '051'=51, '053'=53, '054'=54, '055'=55, '061'=61,
                                                    '062'=62, '063'=63, '064'=64, '065'=65, '066'=66, '067'=67, '068'=68, '069'=69, '071'=71,
                                                    '073'=73, '074'=74, '075'=75, '077'=77, '079'=79, '081'=81, '082'=82, '083'=83, '084'=84,
                                                    '085'=85, '086'=86, '087'=87, '088'=88, '089'=89, '091'=91, '092'=92, '093'=93, '094'=94,
                                                    '095'=95, '096'=96, '097'=97, '098'=98, '099'=99)) #Remove os DDDs com zero na frente
  
  media_ddd<- media_ddd%>% filter(Var1 %in% (11:99)) ##Remove os DDDs incorretos e sem sentido
  return(media_ddd)
}

troca_ddd_por_UF <- function(media_ddd){
  media_ddd$Var1 <- plyr::revalue(media_ddd$Var1, c('11'="SP", '12'="SP", '13'="SP", '14'="SP", '15'="SP", 
                                                    '16'="SP",'17'="SP", '18'="SP", '19'="SP",'21'="RJ",
                                                    '22'="RJ", '24'="RJ", '27'="ES",'28'="ES",
                                                    '31'="MG", '32'="MG", '33'="MG", '34'="MG",
                                                    '35'="MG", '37'="MG", '38'="MG", '41'="PR", 
                                                    '42'="SC", '43'="PR", '44'="PR", '45'="PR", '46'="PR",
                                                    '47'="SC", '48'="SC", '49'="SC", '51'="RS",
                                                    '53'="RS", '54'="RS", '55'="RS", 
                                                    '61'="DF", '62'="GO",'63'="TO", '64'="GO", '65'="MT",
                                                    '66'="MT", '67'="MS", '68'="AC", '69'="RO", '71'="BA",
                                                    '73'="BA", '74'="BA", '75'="BA", '77'="BA", '79'="SE", '81'="PE",
                                                    '82'="AL", '83'="PB", '84'="RN", '85'="CE", '86'="PI",
                                                    '87'="PE", '88'="CE", '89'="PI", '91'="PA", '92'="AM", '93'="PA",
                                                    '94'="PA", '95'="RR", '96'="AP", '97'="AM", '98'="MA", '99'="MA")) 
  #Troca DDD pela sigla
  return(media_ddd)
}


remove_UF_duplicado <- function(media_ddd){
  
  #criar um DF com os estados, limpo:
  id <- c('SP', 'RJ', 'ES', 'MG', 'PR', 'SC', 'RS', 'DF', 'GO', 'TO', 'MT', 'MS', 'AC', 'RO',
          'BA', 'SE', 'PE', 'AL', 'PB', 'RN', 'CE', 'PI', 'PA', 'AM', 'RR', 'AP', 'MA' )
  variavel <- c(0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0)
  df_uf <- data.frame(id, variavel)
  
  #Varre o DF para retirar os UFs repetidos e som�-los
  for(i in 1 : length(media_ddd$Var1)){
    if(media_ddd[[1]][i]=='SP'){
      df_uf[[2]][1] <- df_uf[[2]][1] + media_ddd[[2]][i]
    }
    if(media_ddd[[1]][i]=='RJ'){
      df_uf[[2]][2] <- df_uf[[2]][2] + media_ddd[[2]][i]
    }
    if(media_ddd[[1]][i]=='ES'){
      df_uf[[2]][3] <- df_uf[[2]][3] + media_ddd[[2]][i]
    }
    if(media_ddd[[1]][i]=='MG'){
      df_uf[[2]][4] <- df_uf[[2]][4] + media_ddd[[2]][i]
    }
    if(media_ddd[[1]][i]=='PR'){
      df_uf[[2]][5] <- df_uf[[2]][5] + media_ddd[[2]][i]
    }
    if(media_ddd[[1]][i]=='SC'){
      df_uf[[2]][6] <- df_uf[[2]][6] + media_ddd[[2]][i]
    }
    if(media_ddd[[1]][i]=='RS'){
      df_uf[[2]][7] <- df_uf[[2]][7] + media_ddd[[2]][i]
    }
    if(media_ddd[[1]][i]=='DF'){
      df_uf[[2]][8] <- df_uf[[2]][8] + media_ddd[[2]][i]
    }
    if(media_ddd[[1]][i]=='GO'){
      df_uf[[2]][9] <- df_uf[[2]][9] + media_ddd[[2]][i]
    }
    if(media_ddd[[1]][i]=='TO'){
      df_uf[[2]][10] <- df_uf[[2]][10] + media_ddd[[2]][i]
    }
    if(media_ddd[[1]][i]=='MT'){
      df_uf[[2]][11] <- df_uf[[2]][11] + media_ddd[[2]][i]
    }
    if(media_ddd[[1]][i]=='MS'){
      df_uf[[2]][12] <- df_uf[[2]][12] + media_ddd[[2]][i]
    }
    if(media_ddd[[1]][i]=='AC'){
      df_uf[[2]][13] <- df_uf[[2]][13] + media_ddd[[2]][i]
    }
    if(media_ddd[[1]][i]=='RO'){
      df_uf[[2]][14] <- df_uf[[2]][14] + media_ddd[[2]][i]
    }
    if(media_ddd[[1]][i]=='BA'){
      df_uf[[2]][15] <- df_uf[[2]][15] + media_ddd[[2]][i]
    }
    if(media_ddd[[1]][i]=='SE'){
      df_uf[[2]][16] <- df_uf[[2]][16] + media_ddd[[2]][i]
    }
    if(media_ddd[[1]][i]=='PE'){
      df_uf[[2]][17] <- df_uf[[2]][17] + media_ddd[[2]][i]
    }
    if(media_ddd[[1]][i]=='AL'){
      df_uf[[2]][18] <- df_uf[[2]][18] + media_ddd[[2]][i]
    }
    if(media_ddd[[1]][i]=='PB'){
      df_uf[[2]][19] <- df_uf[[2]][19] + media_ddd[[2]][i]
    }
    if(media_ddd[[1]][i]=='RN'){
      df_uf[[2]][20] <- df_uf[[2]][20] + media_ddd[[2]][i]
    }
    if(media_ddd[[1]][i]=='CE'){
      df_uf[[2]][21] <- df_uf[[2]][21] + media_ddd[[2]][i]
    }
    if(media_ddd[[1]][i]=='PI'){
      df_uf[[2]][22] <- df_uf[[2]][22] + media_ddd[[2]][i]
    }
    if(media_ddd[[1]][i]=='PA'){
      df_uf[[2]][23] <- df_uf[[2]][23] + media_ddd[[2]][i]
    }
    if(media_ddd[[1]][i]=='AM'){
      df_uf[[2]][24] <- df_uf[[2]][24] + media_ddd[[2]][i]
    }
    if(media_ddd[[1]][i]=='RR'){
      df_uf[[2]][25] <- df_uf[[2]][25] + media_ddd[[2]][i]
    }
    if(media_ddd[[1]][i]=='AP'){
      df_uf[[2]][26] <- df_uf[[2]][26] + media_ddd[[2]][i]
    }
    if(media_ddd[[1]][i]=='MA'){
      df_uf[[2]][27] <- df_uf[[2]][27] + media_ddd[[2]][i]
    }
  }
  return(df_uf)
}

data_frame_final <- remove_UF_duplicado(troca_ddd_por_UF(filtra_ddd(media_ddd)))

exibir_mapa_tematico <- function() {
  data_frame_final %>% 
    constroi_mapa_tematico() +
    ggtitle("Pesquisadores por Estado") +
    scale_fill_continuous(name = "", low = '#EEEEEE', high = '#006699', na.value = 'white')
}

#####################################################################################

# As seguintes visualizacoes utilizam o pacote wordcloud (mineracao de texto).

# A principio, os parametros que podem ser modificados pelo usuario sao:
# - max.words (define a quantidade maxima de palavras a ser plotada)
# - min.freq (define a quantidade minima de vezes que uma palavra deve aparecer para ser plotada)

# Caso queira escolher cores diferentes, rodar "display.brewer.all()", escolher o nome da paleta desejada
# e substituir "Dark2" pelo nome da nova paleta

###################### Plotagem nuvem para cidade ######################
wordcloud(
  words = names(table(perfil.df$cidade)), 
  freq = as.numeric(table(perfil.df$cidade)),
  max.words=50,
  random.order=FALSE,
  rot.per=.35,
  scale = c(1, .5),
  min.freq = 4,
  colors=brewer.pal(8, "Dark2"))

###################### Plotagem nuvem para instituicao ######################
wordcloud(
  words = names(table(perfil.df$instituicao)), 
  freq = as.numeric(table(perfil.df$instituicao)),
  max.words=50,
  random.order=FALSE,
  rot.per=.35,
  scale = c(1, .5),
  min.freq = 4,
  colors=brewer.pal(8, "Dark2"))

###################### Plotagem nuvem para orgao ######################
wordcloud(
  words = names(table(perfil.df$orgao)), 
  freq = as.numeric(table(perfil.df$orgao)),
  max.words=50,
  random.order=FALSE,
  rot.per=.35,
  scale = c(1, .5),
  min.freq = 4,
  colors=brewer.pal(8, "Dark2"))

###################### Plotagem nuvem para grande area ######################
wordcloud(
  words = names(table(perfil.areas.df$grande_area)), 
  freq = as.numeric(table(perfil.areas.df$grande_area)),
  max.words=10,
  random.order=FALSE,
  rot.per=.35,
  scale = c(1, .5),
  min.freq = 4,
  colors=brewer.pal(8, "Dark2"))

###################### Plotagem nuvem para area ######################
wordcloud(
  words = names(table(perfil.areas.df$area)), 
  freq = as.numeric(table(perfil.areas.df$area)),
  max.words=50,
  random.order=FALSE,
  rot.per=.35,
  scale = c(1, .5),
  min.freq = 4,
  colors=brewer.pal(8, "Dark2"))

###################### Plotagem nuvem para subarea ######################
wordcloud(
  words = names(table(perfil.areas.df$sub_area)), 
  freq = as.numeric(table(perfil.areas.df$sub_area)),
  max.words=100,
  random.order=FALSE,
  rot.per=.5,
  scale = c(1, .5),
  min.freq = 3,
  colors=brewer.pal(8, "Dark2"))

